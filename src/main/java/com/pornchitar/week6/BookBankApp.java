package com.pornchitar.week6;

public class BookBankApp {
    public static void main(String[] args) {
        BookBank worawit = new BookBank("Worawit", 100.0); //Default Constructor
        worawit.print();
        worawit.deposit(50);
        worawit.print();
        worawit.withdraw(50);
        worawit.print();

        BookBank prayood = new BookBank("Prayood",1);
        prayood.deposit(1000000);
        prayood.withdraw(10000000);
        prayood.print();

        BookBank praweeed = new BookBank("Praweeed",10);
        praweeed.deposit(10000000);
        praweeed.withdraw(1000000);
        praweeed.print();
    }
}
